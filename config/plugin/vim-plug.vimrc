" Install vim-plug if not present
let autoload_plug_path = stdpath('data') . '/site/autoload/plug.vim'
if !filereadable(autoload_plug_path)
	silent execute '!curl -fLo ' . autoload_plug_path . '  --create-dirs
				\ "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"'
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
unlet autoload_plug_path

" Initialize plugin system
call plug#begin(stdpath('data') . '/plugged')

" Big boys
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'github/copilot.vim'
Plug 'yegappan/taglist'

" System integration
Plug 'rbgrouleff/bclose.vim'
Plug 'christoomey/vim-tmux-navigator'

" Theme
Plug 'cocopon/iceberg.vim'

" Python
Plug 'Vimjas/vim-python-pep8-indent'

" GraphQL
Plug 'jparise/vim-graphql'

" OpenWRT
Plug 'cmcaine/vim-uci'

" Other
Plug 'kana/vim-textobj-entire'
Plug 'kana/vim-textobj-user'
Plug 'nelstrom/vim-visual-star-search'
Plug 'tpope/vim-commentary'
Plug 'farmergreg/vim-lastplace'

call plug#end()
