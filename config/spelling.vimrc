let SPELLDIR = fnamemodify(expand("$MYVIMRC"), ":p:h") . '/spell/'
let &spelllang = 'en,cs'
let &spellfile =
	\ SPELLDIR . 'cs.utf-8.add,' .
	\ SPELLDIR . 'cs-jargon.utf-8.add'

augroup spelling
	autocmd!
	autocmd FileType tex let &spellfile .= ',' . SPELLDIR . 'tex.utf-8.add'
augroup end

for d in glob(SPELLDIR . '*.add', 1, 1)
	if filereadable(d) && (!filereadable(d . '.spl') || getftime(d) > getftime(d . '.spl'))
		silent exec 'mkspell! ' . fnameescape(d)
	endif
endfor
