" Create two new lines and switch to insert mode
nnoremap <Leader>j o<CR>
nnoremap <Leader>k O<ESC>O
nnoremap <Leader>o o<ESC>O
nnoremap <Leader>O O<ESC>o

" Disable/enable autocommenting on enter
nnoremap <Leader>c :setlocal formatoptions-=cro<CR>
nnoremap <Leader>C :setlocal formatoptions+=cro<CR>

" TO-BE-FILLED mark navigation
nnoremap <Leader><Space> /<++><Enter>"_c4l

" TODO: Create class function declarations from definitions
" noremap <Leader>f :s/;$/\r{\r\r}\r/<CR>
" noremap <Leader>F :s/\(\w*\)\((.*)\);/<++>::\1\2\r{\r\r}\r/<CR>
