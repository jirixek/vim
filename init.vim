let CONFDIR = fnamemodify(expand("$MYVIMRC"), ":p:h") . '/config/'
let PLUGINDIR = CONFDIR . '/plugin/'

execute 'source' PLUGINDIR . 'vim-plug.vimrc'
execute 'source' CONFDIR . 'general.vimrc'
execute 'source' CONFDIR . 'binds.vimrc'
execute 'source' CONFDIR . 'marks.vimrc'
execute 'source' CONFDIR . 'autocmd.vimrc'
execute 'source' CONFDIR . 'leader.vimrc'
execute 'source' CONFDIR . 'spelling.vimrc'
execute 'source' PLUGINDIR . 'coc.vimrc'
execute 'source' PLUGINDIR . 'vim-tmux-navigator.vimrc'
execute 'source' PLUGINDIR . 'taglist.vimrc'
runtime macros/matchit.vim
