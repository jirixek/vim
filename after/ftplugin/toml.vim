setlocal encoding=utf-8
setlocal fileformat=unix

setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal textwidth=120
setlocal expandtab
setlocal breakindentopt=shift:4     " Shift the wrapped line another four spaces to the right
