setlocal shiftwidth=4
setlocal tabstop=4
setlocal softtabstop=4
setlocal expandtab

augroup json
	autocmd!

	" Allow comments in json files
	autocmd BufRead,BufNewFile *.json setlocal filetype=jsonc
augroup end
